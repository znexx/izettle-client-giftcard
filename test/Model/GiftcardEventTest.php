<?php
/**
 * GiftcardEventTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Giftcard Api
 *
 * Service for managing,selling and redeeming giftcards
 *
 * OpenAPI spec version: Branch:master Commit: 904cdaf Built: 2020-03-30T14:44:04+0000
 * Contact: customers-merchant-sales@izettle.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.12
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * GiftcardEventTest Class Doc Comment
 *
 * @category    Class
 * @description events on giftcard
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class GiftcardEventTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "GiftcardEvent"
     */
    public function testGiftcardEvent()
    {
    }

    /**
     * Test attribute "uuid"
     */
    public function testPropertyUuid()
    {
    }

    /**
     * Test attribute "amount"
     */
    public function testPropertyAmount()
    {
    }

    /**
     * Test attribute "created"
     */
    public function testPropertyCreated()
    {
    }

    /**
     * Test attribute "event_type"
     */
    public function testPropertyEventType()
    {
    }

    /**
     * Test attribute "purchase"
     */
    public function testPropertyPurchase()
    {
    }
}
