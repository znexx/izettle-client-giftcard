<?php
/**
 * RefundApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Giftcard Api
 *
 * Service for managing,selling and redeeming giftcards
 *
 * OpenAPI spec version: Branch:master Commit: 904cdaf Built: 2020-03-30T14:44:04+0000
 * Contact: customers-merchant-sales@izettle.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.12
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Swagger\Client;

use \Swagger\Client\Configuration;
use \Swagger\Client\ApiException;
use \Swagger\Client\ObjectSerializer;

/**
 * RefundApiTest Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class RefundApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for checkGiftcardRefundEligibility
     *
     * verify the giftcard can be refunded or not.
     *
     */
    public function testCheckGiftcardRefundEligibility()
    {
    }

    /**
     * Test case for checkRefundEligibility
     *
     * verify the payment can be refunded or not.
     *
     */
    public function testCheckRefundEligibility()
    {
    }

    /**
     * Test case for refundGiftcard
     *
     * refund the giftcard.
     *
     */
    public function testRefundGiftcard()
    {
    }

    /**
     * Test case for refundPayment
     *
     * refund the payment.
     *
     */
    public function testRefundPayment()
    {
    }
}
