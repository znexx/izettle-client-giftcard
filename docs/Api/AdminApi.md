# Swagger\Client\AdminApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activateOrganization1**](AdminApi.md#activateOrganization1) | **POST** /admin/activation/organization/{uuid}/activate | Activate giftcard for org.
[**cancelGiftcard**](AdminApi.md#cancelGiftcard) | **POST** /admin/organization/{orgUuid}/giftcards/{uuid}/cancel | Cancel giftcard
[**deactivateOrganization**](AdminApi.md#deactivateOrganization) | **POST** /admin/activation/organization/{uuid}/deactivate | deactivate giftcard for org.
[**getAllSettings1**](AdminApi.md#getAllSettings1) | **GET** /admin/organization/{orgUuid}/settings | get All settings for org
[**getGiftcards1**](AdminApi.md#getGiftcards1) | **GET** /admin/organization/{orgUuid}/giftcards | Get all giftcards
[**status1**](AdminApi.md#status1) | **GET** /admin/activation/organization/{uuid} | org status


# **activateOrganization1**
> activateOrganization1($uuid)

Activate giftcard for org.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AdminApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$uuid = "uuid_example"; // string | Organization Identifier as an UUID

try {
    $apiInstance->activateOrganization1($uuid);
} catch (Exception $e) {
    echo 'Exception when calling AdminApi->activateOrganization1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cancelGiftcard**
> \Swagger\Client\Model\Giftcard cancelGiftcard($org_uuid, $uuid)

Cancel giftcard



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AdminApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$org_uuid = "org_uuid_example"; // string | Organization Identifier as an UUID
$uuid = "uuid_example"; // string | Giftcard Identifier as an UUID

try {
    $result = $apiInstance->cancelGiftcard($org_uuid, $uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdminApi->cancelGiftcard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **uuid** | [**string**](../Model/.md)| Giftcard Identifier as an UUID |

### Return type

[**\Swagger\Client\Model\Giftcard**](../Model/Giftcard.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deactivateOrganization**
> deactivateOrganization($uuid)

deactivate giftcard for org.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AdminApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$uuid = "uuid_example"; // string | Organization Identifier as an UUID

try {
    $apiInstance->deactivateOrganization($uuid);
} catch (Exception $e) {
    echo 'Exception when calling AdminApi->deactivateOrganization: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllSettings1**
> \Swagger\Client\Model\SettingsResponse getAllSettings1($org_uuid)

get All settings for org



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AdminApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$org_uuid = "org_uuid_example"; // string | Organization Identifier as an UUID

try {
    $result = $apiInstance->getAllSettings1($org_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdminApi->getAllSettings1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |

### Return type

[**\Swagger\Client\Model\SettingsResponse**](../Model/SettingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getGiftcards1**
> \Swagger\Client\Model\Giftcard[] getGiftcards1($org_uuid)

Get all giftcards



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AdminApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$org_uuid = "org_uuid_example"; // string | Organization Identifier as an UUID

try {
    $result = $apiInstance->getGiftcards1($org_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdminApi->getGiftcards1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |

### Return type

[**\Swagger\Client\Model\Giftcard[]**](../Model/Giftcard.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **status1**
> \Swagger\Client\Model\Activation status1($uuid)

org status



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AdminApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$uuid = "uuid_example"; // string | Organization Identifier as an UUID

try {
    $result = $apiInstance->status1($uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdminApi->status1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |

### Return type

[**\Swagger\Client\Model\Activation**](../Model/Activation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

