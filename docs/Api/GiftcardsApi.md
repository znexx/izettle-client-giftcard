# Swagger\Client\GiftcardsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createGiftcard**](GiftcardsApi.md#createGiftcard) | **POST** /organizations/{organizationUuid}/giftcards | Create giftcard
[**deleteGiftcard**](GiftcardsApi.md#deleteGiftcard) | **DELETE** /organizations/{organizationUuid}/giftcards/{uuid} | Delete giftcard
[**getGiftcard**](GiftcardsApi.md#getGiftcard) | **GET** /organizations/{organizationUuid}/giftcards/{uuid} | Get specified giftcard
[**getGiftcardByCode**](GiftcardsApi.md#getGiftcardByCode) | **GET** /organizations/{organizationUuid}/giftcards/code/{code} | Get specified giftcard using code
[**getGiftcardForExternal**](GiftcardsApi.md#getGiftcardForExternal) | **GET** /organizations/{organizationUuid}/giftcards/external/{uuid} | Get specified giftcard
[**getGiftcards**](GiftcardsApi.md#getGiftcards) | **GET** /organizations/{organizationUuid}/giftcards | Get all giftcards
[**redeemGiftcard**](GiftcardsApi.md#redeemGiftcard) | **POST** /organizations/{organizationUuid}/giftcards/{uuid}/redeem | Redeem giftcard


# **createGiftcard**
> \Swagger\Client\Model\Giftcard createGiftcard($organization_uuid, $body)

Create giftcard



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\GiftcardsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$body = new \Swagger\Client\Model\CreateGiftcardRequest(); // \Swagger\Client\Model\CreateGiftcardRequest | Giftcard data

try {
    $result = $apiInstance->createGiftcard($organization_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GiftcardsApi->createGiftcard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **body** | [**\Swagger\Client\Model\CreateGiftcardRequest**](../Model/CreateGiftcardRequest.md)| Giftcard data | [optional]

### Return type

[**\Swagger\Client\Model\Giftcard**](../Model/Giftcard.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteGiftcard**
> deleteGiftcard($organization_uuid, $uuid)

Delete giftcard



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\GiftcardsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$uuid = "uuid_example"; // string | identifier as and UUID

try {
    $apiInstance->deleteGiftcard($organization_uuid, $uuid);
} catch (Exception $e) {
    echo 'Exception when calling GiftcardsApi->deleteGiftcard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **uuid** | [**string**](../Model/.md)| identifier as and UUID |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getGiftcard**
> \Swagger\Client\Model\Giftcard getGiftcard($organization_uuid, $uuid)

Get specified giftcard



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\GiftcardsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$uuid = "uuid_example"; // string | Giftcard identifier as a UUID

try {
    $result = $apiInstance->getGiftcard($organization_uuid, $uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GiftcardsApi->getGiftcard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **uuid** | [**string**](../Model/.md)| Giftcard identifier as a UUID |

### Return type

[**\Swagger\Client\Model\Giftcard**](../Model/Giftcard.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getGiftcardByCode**
> \Swagger\Client\Model\Giftcard getGiftcardByCode($organization_uuid, $code)

Get specified giftcard using code



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\GiftcardsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$code = "code_example"; // string | Giftcard code

try {
    $result = $apiInstance->getGiftcardByCode($organization_uuid, $code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GiftcardsApi->getGiftcardByCode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **code** | **string**| Giftcard code |

### Return type

[**\Swagger\Client\Model\Giftcard**](../Model/Giftcard.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getGiftcardForExternal**
> \Swagger\Client\Model\ExternalGiftcard getGiftcardForExternal($organization_uuid, $uuid)

Get specified giftcard



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\GiftcardsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$uuid = "uuid_example"; // string | Giftcard identifier as a UUID

try {
    $result = $apiInstance->getGiftcardForExternal($organization_uuid, $uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GiftcardsApi->getGiftcardForExternal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **uuid** | [**string**](../Model/.md)| Giftcard identifier as a UUID |

### Return type

[**\Swagger\Client\Model\ExternalGiftcard**](../Model/ExternalGiftcard.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getGiftcards**
> \Swagger\Client\Model\Giftcard[] getGiftcards($organization_uuid)

Get all giftcards



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\GiftcardsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID

try {
    $result = $apiInstance->getGiftcards($organization_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GiftcardsApi->getGiftcards: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |

### Return type

[**\Swagger\Client\Model\Giftcard[]**](../Model/Giftcard.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **redeemGiftcard**
> \Swagger\Client\Model\PaymentCreated redeemGiftcard($organization_uuid, $uuid, $body)

Redeem giftcard



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\GiftcardsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$uuid = "uuid_example"; // string | Giftcard Identifier as an UUID
$body = new \Swagger\Client\Model\RedeemGiftcardRequest(); // \Swagger\Client\Model\RedeemGiftcardRequest | 

try {
    $result = $apiInstance->redeemGiftcard($organization_uuid, $uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GiftcardsApi->redeemGiftcard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **uuid** | [**string**](../Model/.md)| Giftcard Identifier as an UUID |
 **body** | [**\Swagger\Client\Model\RedeemGiftcardRequest**](../Model/RedeemGiftcardRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PaymentCreated**](../Model/PaymentCreated.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

