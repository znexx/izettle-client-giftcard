# Swagger\Client\SystemApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkRefundEligibility1**](SystemApi.md#checkRefundEligibility1) | **POST** /admin/system/refunds/payments/{paymentUuid}/eligible | verify the payment can be refunded or not


# **checkRefundEligibility1**
> checkRefundEligibility1($payment_uuid, $body)

verify the payment can be refunded or not



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SystemApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$payment_uuid = "payment_uuid_example"; // string | payment Identifier as an UUID
$body = new \Swagger\Client\Model\RefundRequest(); // \Swagger\Client\Model\RefundRequest | 

try {
    $apiInstance->checkRefundEligibility1($payment_uuid, $body);
} catch (Exception $e) {
    echo 'Exception when calling SystemApi->checkRefundEligibility1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payment_uuid** | [**string**](../Model/.md)| payment Identifier as an UUID |
 **body** | [**\Swagger\Client\Model\RefundRequest**](../Model/RefundRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

