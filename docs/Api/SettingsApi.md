# Swagger\Client\SettingsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createOrUpdateDuration**](SettingsApi.md#createOrUpdateDuration) | **POST** /organizations/{organizationUuid}/settings/duration | Create or Update duration settings for org
[**createOrUpdateMaxAmountPerCard**](SettingsApi.md#createOrUpdateMaxAmountPerCard) | **POST** /organizations/{organizationUuid}/settings/maxamountpercard | Create or Update max amount per card settings for org
[**getAllSettings**](SettingsApi.md#getAllSettings) | **GET** /organizations/{organizationUuid}/settings | get All settings for org


# **createOrUpdateDuration**
> \Swagger\Client\Model\DurationSettings createOrUpdateDuration($organization_uuid, $body)

Create or Update duration settings for org



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SettingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$body = new \Swagger\Client\Model\DurationSettings(); // \Swagger\Client\Model\DurationSettings | duration settings data

try {
    $result = $apiInstance->createOrUpdateDuration($organization_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SettingsApi->createOrUpdateDuration: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **body** | [**\Swagger\Client\Model\DurationSettings**](../Model/DurationSettings.md)| duration settings data | [optional]

### Return type

[**\Swagger\Client\Model\DurationSettings**](../Model/DurationSettings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createOrUpdateMaxAmountPerCard**
> \Swagger\Client\Model\MaxAmountPerCardSettings createOrUpdateMaxAmountPerCard($organization_uuid, $body)

Create or Update max amount per card settings for org



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SettingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$body = new \Swagger\Client\Model\MaxAmountPerCardSettingsRequest(); // \Swagger\Client\Model\MaxAmountPerCardSettingsRequest | max amount per card data

try {
    $result = $apiInstance->createOrUpdateMaxAmountPerCard($organization_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SettingsApi->createOrUpdateMaxAmountPerCard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **body** | [**\Swagger\Client\Model\MaxAmountPerCardSettingsRequest**](../Model/MaxAmountPerCardSettingsRequest.md)| max amount per card data | [optional]

### Return type

[**\Swagger\Client\Model\MaxAmountPerCardSettings**](../Model/MaxAmountPerCardSettings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllSettings**
> \Swagger\Client\Model\SettingsResponse getAllSettings($organization_uuid)

get All settings for org



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SettingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID

try {
    $result = $apiInstance->getAllSettings($organization_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SettingsApi->getAllSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |

### Return type

[**\Swagger\Client\Model\SettingsResponse**](../Model/SettingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

