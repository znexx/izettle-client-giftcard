# Swagger\Client\CodesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**generateNewCodes**](CodesApi.md#generateNewCodes) | **GET** /organization/{organizationUuid}/code | generate new codes that can be used when creating gift cards
[**generateNewCodesToFile**](CodesApi.md#generateNewCodesToFile) | **GET** /organization/{organizationUuid}/code/file | generate new codes that can be used when creating gift cards


# **generateNewCodes**
> generateNewCodes($organization_uuid, $number_of_codes)

generate new codes that can be used when creating gift cards

allowed values for numberOfCodes is [1,1000]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$number_of_codes = 1; // int | 

try {
    $apiInstance->generateNewCodes($organization_uuid, $number_of_codes);
} catch (Exception $e) {
    echo 'Exception when calling CodesApi->generateNewCodes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **number_of_codes** | **int**|  | [optional] [default to 1]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **generateNewCodesToFile**
> generateNewCodesToFile($organization_uuid, $number_of_codes)

generate new codes that can be used when creating gift cards

API is intended for file download, allowed values for numberOfCodes [1,1000]

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$number_of_codes = 50; // int | 

try {
    $apiInstance->generateNewCodesToFile($organization_uuid, $number_of_codes);
} catch (Exception $e) {
    echo 'Exception when calling CodesApi->generateNewCodesToFile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **number_of_codes** | **int**|  | [optional] [default to 50]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

