# Swagger\Client\RefundApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkGiftcardRefundEligibility**](RefundApi.md#checkGiftcardRefundEligibility) | **POST** /organizations/{organizationUuid}/giftcards/refunds/{giftcardUuid}/eligible | verify the giftcard can be refunded or not
[**checkRefundEligibility**](RefundApi.md#checkRefundEligibility) | **POST** /organizations/{organizationUuid}/giftcards/refunds/payments/{paymentUuid}/eligible | verify the payment can be refunded or not
[**refundGiftcard**](RefundApi.md#refundGiftcard) | **POST** /organizations/{organizationUuid}/giftcards/refunds/{giftcardUuid} | refund the giftcard
[**refundPayment**](RefundApi.md#refundPayment) | **POST** /organizations/{organizationUuid}/giftcards/refunds/payments/{paymentUuid} | refund the payment


# **checkGiftcardRefundEligibility**
> checkGiftcardRefundEligibility($organization_uuid, $giftcard_uuid)

verify the giftcard can be refunded or not



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RefundApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$giftcard_uuid = "giftcard_uuid_example"; // string | giftcard Identifier as an UUID

try {
    $apiInstance->checkGiftcardRefundEligibility($organization_uuid, $giftcard_uuid);
} catch (Exception $e) {
    echo 'Exception when calling RefundApi->checkGiftcardRefundEligibility: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **giftcard_uuid** | [**string**](../Model/.md)| giftcard Identifier as an UUID |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **checkRefundEligibility**
> checkRefundEligibility($organization_uuid, $payment_uuid, $body)

verify the payment can be refunded or not



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RefundApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$payment_uuid = "payment_uuid_example"; // string | payment Identifier as an UUID
$body = new \Swagger\Client\Model\RefundRequest(); // \Swagger\Client\Model\RefundRequest | 

try {
    $apiInstance->checkRefundEligibility($organization_uuid, $payment_uuid, $body);
} catch (Exception $e) {
    echo 'Exception when calling RefundApi->checkRefundEligibility: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **payment_uuid** | [**string**](../Model/.md)| payment Identifier as an UUID |
 **body** | [**\Swagger\Client\Model\RefundRequest**](../Model/RefundRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **refundGiftcard**
> \Swagger\Client\Model\Giftcard refundGiftcard($organization_uuid, $giftcard_uuid, $body)

refund the giftcard



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RefundApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$giftcard_uuid = "giftcard_uuid_example"; // string | giftcard Identifier as an UUID
$body = new \stdClass; // object | 

try {
    $result = $apiInstance->refundGiftcard($organization_uuid, $giftcard_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RefundApi->refundGiftcard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **giftcard_uuid** | [**string**](../Model/.md)| giftcard Identifier as an UUID |
 **body** | **object**|  | [optional]

### Return type

[**\Swagger\Client\Model\Giftcard**](../Model/Giftcard.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **refundPayment**
> \Swagger\Client\Model\PaymentCreated refundPayment($organization_uuid, $payment_uuid, $body)

refund the payment



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RefundApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$payment_uuid = "payment_uuid_example"; // string | payment Identifier as an UUID
$body = new \Swagger\Client\Model\RefundRequest(); // \Swagger\Client\Model\RefundRequest | 

try {
    $result = $apiInstance->refundPayment($organization_uuid, $payment_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RefundApi->refundPayment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **payment_uuid** | [**string**](../Model/.md)| payment Identifier as an UUID |
 **body** | [**\Swagger\Client\Model\RefundRequest**](../Model/RefundRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PaymentCreated**](../Model/PaymentCreated.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

