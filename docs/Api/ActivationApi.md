# Swagger\Client\ActivationApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activateOrganization**](ActivationApi.md#activateOrganization) | **POST** /activation/{organizationUuid}/activate | Activate giftcard for org.
[**status**](ActivationApi.md#status) | **GET** /activation/{organizationUuid} | org status


# **activateOrganization**
> activateOrganization($organization_uuid)

Activate giftcard for org.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ActivationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID

try {
    $apiInstance->activateOrganization($organization_uuid);
} catch (Exception $e) {
    echo 'Exception when calling ActivationApi->activateOrganization: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **status**
> \Swagger\Client\Model\Activation status($organization_uuid)

org status



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ActivationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID

try {
    $result = $apiInstance->status($organization_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ActivationApi->status: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |

### Return type

[**\Swagger\Client\Model\Activation**](../Model/Activation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

