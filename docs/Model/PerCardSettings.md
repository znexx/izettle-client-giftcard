# PerCardSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_amount** | **int** |  | 
**min_amount** | **int** |  | 
**currency** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


