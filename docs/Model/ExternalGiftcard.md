# ExternalGiftcard

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **string** |  | [optional] 
**created** | [**\DateTime**](\DateTime.md) | Date when the giftcard was created | [optional] 
**valid_to** | [**\DateTime**](\DateTime.md) | Date when the giftcard validity ends | [optional] 
**initial_amount** | **int** |  | 
**remaining_amount** | **int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


