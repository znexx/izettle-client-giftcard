# PaymentCreated

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | 
**uuid** | **string** |  | 
**receiver_organization** | **string** |  | 
**amount** | **int** |  | 
**currency** | **string** |  | 
**country** | **string** |  | 
**created_at** | **int** |  | 
**details** | [**\Swagger\Client\Model\MessageDetails**](MessageDetails.md) |  | 
**references** | **map[string,object]** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


