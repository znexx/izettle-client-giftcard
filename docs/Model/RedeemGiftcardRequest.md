# RedeemGiftcardRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**redeem_amount** | **int** |  | 
**checkout_uuid** | **string** |  | 
**currency_id** | **string** |  | 
**country_id** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


