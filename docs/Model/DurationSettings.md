# DurationSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**no_of_years** | **int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


