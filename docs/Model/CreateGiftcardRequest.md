# CreateGiftcardRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **string** |  | 
**code** | **string** | Code for giftcard | 
**valid_from** | **int** | Time when the giftcard validity starts | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


