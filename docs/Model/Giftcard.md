# Giftcard

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **string** |  | 
**code** | **string** | Code for giftcard | 
**valid_from** | **int** | Time from when the giftcard is valid | 
**valid_to** | **int** | Time when the giftcard validity ends | 
**status** | **string** |  | 
**initial_amount** | **int** |  | 
**remaining_amount** | **int** |  | 
**events** | [**\Swagger\Client\Model\GiftcardEvent[]**](GiftcardEvent.md) |  | 
**currency** | **string** | giftcard currency. null for inactive giftcard | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


