# GiftcardEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **string** |  | 
**amount** | **int** |  | 
**created** | **int** |  | 
**event_type** | **string** |  | 
**purchase** | [**\Swagger\Client\Model\GiftcardPurchase**](GiftcardPurchase.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


