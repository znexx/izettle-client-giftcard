# SettingsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**duration** | [**\Swagger\Client\Model\DurationSettings**](DurationSettings.md) |  | [optional] 
**per_card_setting** | [**\Swagger\Client\Model\PerCardSettings**](PerCardSettings.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


